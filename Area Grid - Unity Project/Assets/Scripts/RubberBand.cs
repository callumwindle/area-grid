﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class RubberBand : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    private RectTransform rectTransform;
    private CanvasGroup canvasGroup;
    public Canvas canvas;
    public GridChanger gcScript;
    
    // Start is called before the first frame update
    void Start()
    {
        rectTransform = GetComponent<RectTransform>();
        canvasGroup = GetComponent<CanvasGroup>();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        canvasGroup.blocksRaycasts = false;
        canvasGroup.alpha = 0.75f;
    }

    public void OnDrag(PointerEventData eventData)
    {
        rectTransform.anchoredPosition += (eventData.delta/canvas.scaleFactor);
        if(gcScript.isScaling)
        {
            switch(gameObject.transform.GetSiblingIndex())
            {
                case 0:
                    gameObject.transform.parent.GetChild(3).gameObject.GetComponent<RectTransform>().anchoredPosition += new Vector2((eventData.delta/canvas.scaleFactor).x,0f);
                    gameObject.transform.parent.GetChild(1).gameObject.GetComponent<RectTransform>().anchoredPosition += new Vector2(0,(eventData.delta/canvas.scaleFactor).y);
                break;

                case 1:
                    gameObject.transform.parent.GetChild(2).gameObject.GetComponent<RectTransform>().anchoredPosition += new Vector2((eventData.delta/canvas.scaleFactor).x,0f);
                    gameObject.transform.parent.GetChild(0).gameObject.GetComponent<RectTransform>().anchoredPosition += new Vector2(0,(eventData.delta/canvas.scaleFactor).y);
                break;

                case 2:
                    gameObject.transform.parent.GetChild(1).gameObject.GetComponent<RectTransform>().anchoredPosition += new Vector2((eventData.delta/canvas.scaleFactor).x,0f);
                    gameObject.transform.parent.GetChild(3).gameObject.GetComponent<RectTransform>().anchoredPosition += new Vector2(0,(eventData.delta/canvas.scaleFactor).y);
                break;

                case 3:
                    gameObject.transform.parent.GetChild(0).gameObject.GetComponent<RectTransform>().anchoredPosition += new Vector2((eventData.delta/canvas.scaleFactor).x,0f);
                    gameObject.transform.parent.GetChild(2).gameObject.GetComponent<RectTransform>().anchoredPosition += new Vector2(0,(eventData.delta/canvas.scaleFactor).y);
                break;
            }
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        canvasGroup.blocksRaycasts = true;
        canvasGroup.alpha = 1f;
    }
}
