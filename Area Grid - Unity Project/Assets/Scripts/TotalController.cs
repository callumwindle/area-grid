﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TotalController : MonoBehaviour
{

    public float areaTotal;
    public Text displayText, unitButtonText, showButtonText;
    private bool meterSquared, showTotal;
    private string currUnits;
    public List<GridBoxScript> boxScriptList = new List<GridBoxScript>();
    // Start is called before the first frame update
    void Start()
    {
        areaTotal = 0;
        showTotal = false;
        meterSquared = false;
        currUnits = "cm²";

        UpdateDisplay();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateDisplay()
    {
        if(showTotal)
        {
            displayText.text = areaTotal.ToString() + currUnits;
        }
        else
        {
            displayText.text = "??" + currUnits;
        }
    }

    public void CycleUnits()
    {
        meterSquared = !meterSquared;

        if(meterSquared)
        {
            currUnits = "m²";
        }
        else
        {
            currUnits = "cm²";
        }

        UpdateDisplay();
    }

    public void CycleAnswer()
    {
        showTotal = !showTotal;

        if(showTotal)
        {
            showButtonText.text = "Hide";
        }
        else
        {
            showButtonText.text = "Show";
        }

        UpdateDisplay();
    }

    public void ReCount()
    {
        areaTotal = 0;
        foreach(GridBoxScript i in boxScriptList)
        {
            areaTotal += i.currBoxVal;
        }
        UpdateDisplay();
    }
}
