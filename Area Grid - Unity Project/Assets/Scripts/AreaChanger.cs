﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AreaChanger : MonoBehaviour
{
    public List<GameObject> squarePoints = new List<GameObject>();
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        for(int i=0; i < squarePoints.Count; i++)
        {
            if(i != (squarePoints.Count -1))
            {
                RenderLine(squarePoints[i], squarePoints[i+1]);
            }
            else
            {
                RenderLine(squarePoints[i], squarePoints[0]);
            }
        }
    }

    public void RenderLine(GameObject x, GameObject y)
    {
        x.GetComponent<LineRenderer>().SetPosition(0, x.transform.position);
        x.GetComponent<LineRenderer>().SetPosition(1, y.transform.position);
    }
}
