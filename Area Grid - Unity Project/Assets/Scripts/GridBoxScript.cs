﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class GridBoxScript : MonoBehaviour, IPointerDownHandler
{
    public GridChanger gridChanger;
    public int gridColour, gridShapeNum;
    public float currBoxVal;
    public GameObject gridShape;
    public TotalController tcScript;
    // Start is called before the first frame update
    void Start()
    {
        ResetBox();
        if(transform.parent.transform.parent.transform.parent.Find("Total Panel") != null)
        {
            tcScript = transform.parent.transform.parent.transform.parent.Find("Total Panel").gameObject.GetComponent<TotalController>();
        }
    }

    public void ResetBox()
    {
        transform.GetComponent<Image>().color = gridChanger.colourList[0];
        currBoxVal = 0;
        gridColour = 0;
        gridShapeNum = 69;


        if(transform.childCount > 0)
        {
            for(int i = 0; i < (transform.childCount); i++)
            {
                Destroy(transform.GetChild(i).gameObject);
            }
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {      
        if(gridShapeNum != gridChanger.currShape)
        {
            Destroy(gridShape);
            
            gridShape = Instantiate(gridChanger.shapeTemplate[gridChanger.currShape]);
            if(tcScript != null)
            {
                EditTotal(gridChanger.currShape);
            }
            gridShape.GetComponent<Image>().color = gridChanger.colourList[gridChanger.currColour];
            gridShape.transform.SetParent(transform, false);
            gridShape.GetComponent<RectTransform>().anchorMin = new Vector2 (0f, 0f);
            gridShape.GetComponent<RectTransform>().anchorMax = new Vector2 (1f, 1f);
            gridShape.SetActive(true);
            gridShapeNum = gridChanger.currShape;
            gridColour = gridChanger.currColour;
        }
        else if(gridColour != gridChanger.currColour && gridShapeNum == gridChanger.currShape)
        {
            transform.GetChild(0).gameObject.GetComponent<Image>().color = gridChanger.colourList[gridChanger.currColour];
            gridColour = gridChanger.currColour;
        }
        else
        {
            Destroy(gridShape);
            gridColour = 0;
            gridShapeNum = 69;
            if(tcScript != null)
            {
                EditTotal(-1);
            }
        }
    }

    public void EditTotal(int shapeVal)
    {
        tcScript.areaTotal -= currBoxVal;
        switch(shapeVal)
        {
            case -1:
                currBoxVal = 0;
            break;
            
            case 0:
                tcScript.areaTotal += 1;
                currBoxVal = 1;
            break;
            
            case 1:
                tcScript.areaTotal += 0.5f;
                currBoxVal = 0.5f;
            break;

            case 2:
                tcScript.areaTotal += 0.5f;
                currBoxVal = 0.5f;
            break;

            case 3:
                tcScript.areaTotal += 0.5f;
                currBoxVal = 0.5f;
            break;

            case 4:
                tcScript.areaTotal += 0.5f;
                currBoxVal = 0.5f;
            break;
        }

        tcScript.UpdateDisplay();
    }
}
